<!--
 koto is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 koto is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with koto. If not, see <https://www.gnu.org/licenses/>.
-->

# Koto

API Testing tool, just like Postman/Insomnia/Bruno, for CLI power users.

| Language          | status        |
|-------------------|---------------|
| [GNU Bash](bash)  | mostly done   |
| [Python3](python) | initial steps |
| [Golang](golang)  | initial steps |

## Configuration

`koto` reads the `koto.yaml` in project root and generate actions queries.

| Framework | Where                                    |
|-----------|------------------------------------------|
| actions   | Actions describe endpoints to be listed  |
| config    | overwrite miscellaneous configurations   |
| security  | string/path/url of sensitive information |
    

![koto](koto.png)

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)

