<!--
 koto is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 koto is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with koto. If not, see <https://www.gnu.org/licenses/>.
-->

# Koto | GNU Bash

API Testing tool, just like Postman/Insomnia/Bruno, for CLI power users.

### USAGE

    ./koto.bash
    ./koto.bash --id 29fe4068-fdf8-4775-acc0-140c4d066612
    ./koto.bash --header json
    ./koto.bash --security 
    ./koto.bash --verbose

   
### DEPENDENCIES

Check for dependencies in [koto.bash](koto.bash).

## [GNU Guix](https://guix.gnu.org)

You can dry-run `koto` without messing with host system configuration with [GNU Shell](https://guix.gnu.org/manual/en/html_node/Invoking-guix-shell.html). 

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)

