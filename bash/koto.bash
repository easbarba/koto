#!/usr/bin/env bash

# Koto is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Koto is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koto. If not, see <https://www.gnu.org/licenses/>.

# ====== USAGE

# $ ./koto.bash
# $ ./koto.bash --id 29fe4068-fdf8-4775-acc0-140c4d066612
# $ ./koto.bash --security
# $ ./koto.bash --header json
# $ ./koto.bash --verbose

# ====== GENERAL INFORMATION
# https://www.gnu.org/software/bash/manual/bash.html#Brace-Expansion

# DEBUG SETTINGS
set -euo pipefail

# ====== DEPENDENCIES
DEPS=(bash curl fzf awk yq)
for dep in "${DEPS[@]}"; do
    [[ -z $(command -v "$dep") ]] && echo "$dep not found, exiting!" && exit
done

# ============================== CLI

# VARIABLES
declare -A OPTIONS=([verbose]=false [id]=false [security]=false)

print_usage() {
    echo -e "Usage: koto [options]
    -i ID,   --id ID                 resource id.
    -s,      --security              use security file with variables
    -v,      --verbose               display as much as information as possible."

    exit 0
}

# -- OPTIONS:

# REPLACE LONG ARGUMENTS
for arg in "$@"; do
    shift
    case "$arg" in
    '--help') set -- "$@" '-h' ;;
    '--id') set -- "$@" '-i' ;;
    '--security') set -- "$@" '-s' ;;
    '--verbose') set -- "$@" '-v' ;;
    *) set -- "$@" "$arg" ;;
    esac
done

OPTIND=1
while getopts "hvsc:i:" FLAG; do
    case $FLAG in
    'h') print_usage ;;
    'i') OPTIONS[id]=$OPTARG ;;
    's') OPTIONS[security]=true ;;
    'v') OPTIONS[verbose]=true ;;
    *) print_usage ;;
    esac
done
shift $((OPTIND - 1))

# ====== BASE

CONFIG_FILE="$PWD/koto.yaml"

# SECURITY INFO
ADMIN_NAME=$(yq e '.security.ADMIN_NAME' "$CONFIG_FILE")
ADMIN_PASSWORD=$(yq e '.security.ADMIN_PASSWORD' "$CONFIG_FILE")

echo $ADMIN_NAME
exit

# PATH DEFAULTS
PORT="5000"
VERSION="v1"

if [[ -f $CONFIG_FILE ]]; then
    FINAL_PATH=$(awk --field-separator '=' '/FINAL_PATH/ { print $2; exit }' "$CONFIG_FILE")
else
    FINAL_PATH="localhost:$PORT/api/$VERSION"
fi

ACTION=''
if [[ ${OPTIONS[id]} == false ]]; then
    ACTION="$(awk '!/ID/' "$ACTIONS_FILE" | fzf)" # list all non-ID actions
else
    ACTION="$(awk 'BEGIN{ entry=$1; } { if(entry !=  $1); print }' "$ACTIONS_FILE" | fzf)" # list ID-only entries
fi

ENTRIES="${ACTION##*: }"
HTTP_METHOD="${ENTRIES%% *}"
PATH_VARIABLE="${ENTRIES#* }"

# FINAL COMMAND
FINAL_COMMAND="$FINAL_PATH$PATH_VARIABLE"

# Add resource ID if requested so
[[ -n ${OPTIONS[id]} ]] && FINAL_COMMAND="${FINAL_COMMAND/ID/"${OPTIONS[id]}"}"

verbose() {
    local information="Koto
LICENSE: GNU GENERAL PUBLIC LICENSE Version 3
METHOD: $HTTP_METHOD
PATH: $PATH_VARIABLE
PORT: $PORT
VERSION: $VERSION
COMMAND: $FINAL_COMMAND

-- OUTPUT --
"
    echo -e "$information"
}

# ================== RUN

GETTER=curl
HEADER="accept: application/json"
[[ ${OPTIONS[verbose]} == true ]] && GETTER="$GETTER --verbose" && verbose
[[ ${OPTIONS[security]} == true ]] && GETTER="$GETTER --user $ADMIN_NAME:$ADMIN_PASSWORD"

$GETTER \
    --header "$HEADER" \
    --request "$HTTP_METHOD" \
    --url "$FINAL_COMMAND"

exit 0
