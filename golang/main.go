/*
* koto is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* koto is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with koto. If not, see <https://www.gnu.org/licenses/>.
**/

package main

import (
	"fmt"
	"log"
	"os"

	"gopkg.in/yaml.v3"
	"github.com/charmbracelet/huh"
)


func main() {
	formsResult()
	makeRequest()
}

func formsResult() {
	var result string

	config := configParse()
	for key, endpoints := range config.Actions {
		var groups []*huh.Group
		var options []huh.Option[string]
		for _, endpoint := range endpoints {
			options = append(options, huh.NewOption(endpoint.Name, endpoint.Name))
		}

		groups = append(groups, huh.NewGroup(huh.NewSelect[string]().
			Title(key).
			Options(options...).
			Value(&result)),
		)

		form := huh.NewForm(groups...)
		err := form.Run()
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Action: ", result)
	}
}

type APIEndpoint struct {
	Name            string   `yaml:"name"`
	Method          string   `yaml:"method"`
	Endpoint        string   `yaml:"endpoint"`
	Description     string   `yaml:"description"`
	QueryParameters []string `yaml:"query_parameters,omitempty"`
}

type Config struct {
	FinalPath string `yaml:"final_path"`
}

type Security struct {
	AdminName     string `yaml:"ADMIN_NAME"`
	AdminPassword string `yaml:"ADMIN_PASSWORD"`
}

type Settings struct {
	Actions  map[string][]APIEndpoint `yaml:"actions"`
	Config   Config                   `yaml:"config"`
	Security Security                 `yaml:"security"`
}

func configParse() Settings {
	yamlContent, err := os.ReadFile("koto.yaml")
	if err != nil {
		fmt.Printf("Error parsing YAML: %v\n", err)
		os.Exit(1)
	}

	var config Settings
	err = yaml.Unmarshal([]byte(yamlContent), &config)
	if err != nil {
		fmt.Printf("Error parsing YAML: %v\n", err)
		os.Exit(1)
	}
	return config
}

func makeRequest() {
	fmt.Println("fool")
}
