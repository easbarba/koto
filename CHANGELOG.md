<!--
 koto is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 koto is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with koto. If not, see <https://www.gnu.org/licenses/>.
-->

# CHANGELOG

## 0.4.0

- feat: python - select endpoint 
- feat: parse yaml and generate actions

## 0.3.0

- feat: add configuration file examples
- feat: add example

## 0.2.0

- feat: use curl
- feat: add most commands
- feat: add cli

## 0.1.0

- initial structure
