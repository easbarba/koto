# koto is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# koto is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with koto. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from typing import Any
import yaml

import inquirer

config_location: Path = Path().parent.joinpath("koto.yaml")

with open(config_location, "r") as file:
    data: Any = yaml.safe_load(file)  # Use safe_load to avoid security risks

for actions in data["actions"].items():
    f = actions[0]
    m = [d["name"] for d in actions[1]]
    questions = [
        inquirer.List(
            f,
            message=f"{f} endpoints",
            choices=m,
        ),
    ]
    answer = inquirer.prompt(questions)

    for d in actions[1]:
        if d["name"] == answer.get(f):
            print(f"response: {d.get('endpoint')}")
